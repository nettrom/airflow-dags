"""
### Aggregates Projectview and Archives to Legacy Format.
* This job is responsible for aggregating projectview from pageview,
* and then transform and archive this data into legacy format.

Ouput
* Output is appended into (year, month, day, hour) partitions in /wmf/data/wmf/projectview/hourly,
* and then archived into /wmf/data/archive/projectview/legacy/hourly

### Note
* This job waits pageview input partition data for that hour to be available before performing aggregation task.
* Runs a hive query to aggregate from pageview into projectview.
* The transforms projectview to legacy format.
* Archives the data by moving to achive directory.
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from analytics.config.dag_config import (
    archive_directory,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "projectview_hourly"
var_props = VariableProperties(f"{dag_id}_config")
source_table = "wmf.pageview_hourly"
destination_table = "wmf.projectview_hourly"
year = "{{data_interval_start.year}}"
month = "{{data_interval_start.month}}"
day = "{{data_interval_start.day}}"
hour = "{{data_interval_start.hour}}"

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 9, 26)),
    schedule="@hourly",
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=5),
        },
    ),
    user_defined_filters=filters,
    tags=[
        "hourly",
        "from_hive",
        "to_hdfs",
        "to_hive",
        "uses_archiver",
        "uses_hql",
        "requires_wmf_pageview_hourly",
    ],
) as dag:
    sensor = NamedHivePartitionSensor(
        task_id="wait_for_pageview", partition_names=[f"{source_table}/year={year}/month={month}/day={day}/hour={hour}"]
    )

    etl1 = SparkSqlOperator(
        task_id="aggregrate_pageview_to_projectview",
        sql=var_props.get("hql_path", f"{hql_directory}/projectview/hourly/aggregate_pageview_to_projectview.hql"),
        query_parameters={
            "source_table": source_table,
            "destination_table": destination_table,
            "record_version": var_props.get("record_version", "0.0.1"),
            "year": year,
            "month": month,
            "day": day,
            "hour": hour,
            "coalesce_partitions": 1,
        },
    )

    temporary_directory = var_props.get(
        "temporary_directory",
        f"{hadoop_name_node}/wmf/tmp/analytics/{dag_id}/{{{{data_interval_start|to_ds_hour_nodash}}}}",
    )

    etl2 = SparkSqlOperator(
        task_id="transform_projectview",
        sql=var_props.get(
            "hql_path2", f"{hql_directory}/projectview/hourly/transform_projectview_to_legacy_format.hql"
        ),
        query_parameters={
            "source_table": destination_table,
            "destination_directory": temporary_directory,
            "year": year,
            "month": month,
            "day": day,
            "hour": hour,
            "coalesce_partitions": 1,
        },
    )

    etl3 = HDFSArchiveOperator(
        task_id="move_data_to_archive",
        source_directory=temporary_directory,
        archive_file=var_props.get(
            "archive_file",
            f"{archive_directory}/projectview/legacy/hourly/"
            + "{{data_interval_start.add(hours=1).year}}/{{data_interval_start|add_hours(1)|to_ds_month}}/projectviews"
            + "-{{data_interval_start|add_hours(1)|to_ds_nodash}}-{{data_interval_start|add_hours(1)|to_time_nodash}}",
        ),
        expected_filename_ending=".csv",  # gets written by spark with .csv extension
        check_done=True,
    )

    sensor >> etl1 >> etl2 >> etl3
