# Multi stage Dockerfile.
#
# Stages:
# - 1. conda_dist: Installs a Conda distribution into Debian
# - 2. conda_env_prod: Creates a Conda environment for production
# - 3.a. conda_env_test: Creatas a Conda environment for pytest
# - 3.b. conda_pack: Pack the Conda environment
# - 4. build_deb: Build the debian package
# - 5. test_deb: Test the debian package
# - 6. conda_env_lint: Build conda environment containing only linting deps
# - 7. publish_deb: Publish deb package to registry
#
# Example of command to build locally:
# docker build \
#   --platform linux/x86_64 \
#   -m 8g \
#   -c 5 \
#   -f debian/Dockerfile \
#   --target conda_env_test \
#   -t test_build .



# Declare global "ARG" values. These will be shared between all build stages.
ARG PACKAGE_NAME=airflow
ARG PACKAGE_VERSION="2.6.3-py3.10-20230711"
# The Python version should match the one from the conda-environment.lock.yml
ARG PYTHON_VERSION="3.10.9"
ARG WORK_DIR=/srv/${PACKAGE_NAME}
# Debian Bullseye version may be updated from .gitlab-ci.yml.
ARG BULLSEYE_VERSION=20230528

ARG DEBIAN_DIR=${WORK_DIR}/debian
# Debian tree that will be installed: /srv/airflow/debian/airflow
ARG DEBIAN_PACKAGE_TREE=${DEBIAN_DIR}/${PACKAGE_NAME}
# Path in the debian tree to unpack the conda env
ARG DEBIAN_CONDA_ENV_PATH=${DEBIAN_PACKAGE_TREE}/usr/lib/${PACKAGE_NAME}
ARG ARCH=amd64
ARG DEB_FILE="${WORK_DIR}/${PACKAGE_NAME}-${PACKAGE_VERSION}_${ARCH}.deb"



#
# == Stage 1: conda_dist - Build conda_dist_env.tgz
#

FROM docker-registry.wikimedia.org/bullseye:${BULLSEYE_VERSION} as conda_dist

# Declare the ARGs used for this build stage.
ARG PACKAGE_NAME

# Get apt dependencies
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN echo "krb5-config krb5-config/default_realm string default" | debconf-set-selections && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends krb5-config \
                                               libkrb5-dev && \
    apt-get install -y --no-install-recommends curl \
                                               ca-certificates \
                                               git \
                                               gcc \
                                               build-essential \
                                               sqlite3 \
                                               libsasl2-dev && \
    rm -rf /var/lib/apt/lists/*

# Get Miniconda
ARG MINICONDA_INSTALLER=https://repo.anaconda.com/miniconda/Miniconda3-py310_23.3.1-0-Linux-x86_64.sh
ARG MINICONDA_INSTALLER_MD5=e65ad52d60452ce818869c3309d7964e
RUN curl -o /tmp/miniconda_installer.sh $MINICONDA_INSTALLER && \
  echo "${MINICONDA_INSTALLER_MD5}  /tmp/miniconda_installer.sh" > /tmp/miniconda_installer.md5 && \
  cat /tmp/miniconda_installer.md5 && \
  md5sum --check --strict /tmp/miniconda_installer.md5 && \
  chmod +x /tmp/miniconda_installer.sh && \
  /tmp/miniconda_installer.sh -b -p /opt/miniconda && \
  rm /tmp/miniconda_installer.sh



#
# == Stage 2: conda_env_prod: Creates a Conda environment for production
#

FROM conda_dist as conda_env_prod

# Declare the ARGs used for this build stage.
ARG WORK_DIR
ARG PACKAGE_NAME

# Get dependencies files to build the production Conda environment
RUN mkdir -p "${WORK_DIR}"
WORKDIR ${WORK_DIR}
COPY conda-environment.lock.yml "${WORK_DIR}/"

# Create the production Conda environment
RUN source /opt/miniconda/bin/activate && \
  conda install -n base conda-libmamba-solver && \
  conda config --set solver libmamba && \
  conda env create --quiet --file conda-environment.lock.yml --name ${PACKAGE_NAME}



#
# == Stage 3.a. conda_env_test: Creatas a Conda environment for testing and linting purpose
#
FROM conda_env_prod as conda_env_test

# Declare the ARGs used for this build stage.
ARG WORK_DIR
ARG PACKAGE_NAME

# Get dependencies files to build the test Conda environment
WORKDIR ${WORK_DIR}
COPY pyproject.toml "${WORK_DIR}/"

# Add test dependencies to the environment & remove unused files in test
RUN source /opt/miniconda/bin/activate && \
  conda activate ${PACKAGE_NAME} && \
  pip install --quiet .[test] && \
  rm /opt/miniconda/envs/${PACKAGE_NAME}/share/py4j/py4j0.10.9.jar \
    /opt/miniconda/envs/${PACKAGE_NAME}/lib/python3.10/site-packages/skein/java/skein.jar \
    /opt/miniconda/envs/${PACKAGE_NAME}/lib/python3.10/site-packages/pyspark/jars/* && \
  rm -Rf /opt/miniconda/pkgs



#
# == Stage 3.b. conda_pack: Pack the production Conda environment
#
FROM conda_env_prod as conda_pack

# Declare the ARGs used for this build stage.
ARG WORK_DIR
ARG PACKAGE_NAME

WORKDIR ${WORK_DIR}

# Build the conda_dist_env.tgz
RUN source /opt/miniconda/bin/activate && \
  conda activate base && \
  pip install conda-pack@git+https://gitlab.wikimedia.org/repos/data-engineering/conda-pack.git@master && \
  conda-pack \
    --name ${PACKAGE_NAME} \
    --output ${PACKAGE_NAME}.tgz \
    --compress-level 0 \
    --n-threads -1

# Keep downloaded packages conda cache but remove tarballs
# hadolint ignore=SC3009
RUN rm /opt/miniconda/pkgs/*.{conda,tar.bz2} && \
    mv /opt/miniconda/pkgs ${WORK_DIR}/pkgs

# Remove no longer needed files.
RUN rm -r /opt/miniconda

CMD ["/bin/bash"]



#
# == Stage 4. build_deb: bundle the packed conda environment it into a debian package
#
FROM docker-registry.wikimedia.org/bullseye:${BULLSEYE_VERSION} as build_deb

# Declare the ARGs used for this build stage.
ARG WORK_DIR
ARG PACKAGE_NAME
ARG DEBIAN_DIR
ARG DEBIAN_PACKAGE_TREE
ARG DEBIAN_CONDA_ENV_PATH
ARG DEB_FILE

# Get apt dependencies
RUN apt-get update && \
  apt-get install -y --no-install-recommends fakeroot \
                                             ca-certificates \
                                             build-essential \
                                             debhelper && \
  rm -rf /var/lib/apt/lists/*

# Copy the conda_dist_env.tgz made from the previous build stage
COPY --from=conda_pack ${WORK_DIR}/${PACKAGE_NAME}.tgz ${WORK_DIR}/
COPY --from=conda_pack ${WORK_DIR}/pkgs ${WORK_DIR}/pkgs

# Make directories and copy in files needed to build the .deb.
RUN mkdir -p \
  ${DEBIAN_DIR}/bin \
  ${DEBIAN_DIR}/DEBIAN \
  ${DEBIAN_PACKAGE_TREE}/DEBIAN \
  ${DEBIAN_CONDA_ENV_PATH}
COPY debian/changelog ${DEBIAN_DIR}/
COPY debian/control ${DEBIAN_DIR}/
COPY debian/compat ${DEBIAN_DIR}/
COPY debian/copyright ${DEBIAN_DIR}/
COPY debian/postinst ${DEBIAN_PACKAGE_TREE}/DEBIAN/
RUN chmod 755 ${DEBIAN_PACKAGE_TREE}/DEBIAN/postinst
COPY debian/changelog ${DEBIAN_PACKAGE_TREE}/usr/share/doc/airflow/changelog
COPY debian/copyright ${DEBIAN_PACKAGE_TREE}/usr/share/doc/airflow/copyright
# Optimize changelog & copyright size
RUN gzip -v --best "${DEBIAN_PACKAGE_TREE}/usr/share/doc/airflow/changelog" && \
  gzip -v --best "${DEBIAN_PACKAGE_TREE}/usr/share/doc/airflow/copyright"

# Unpack the conda dist ARG into its install path in the debian package tree.
RUN tar zxf ${WORK_DIR}/airflow.tgz -C "${DEBIAN_CONDA_ENV_PATH}"

# Warm the `pkgs` cache folder used when cloning (with run_dev_instance.sh)
# pkgs could be rebuild with:
#    conda list --explicit > installed_packages_list.text
#    CONDA_PKGS_DIRS="${DEBIAN_CONDA_ENV_PATH}/pkgs"
#    conda install --download-only --file installed_packages_list.txt
RUN mv "${WORK_DIR}/pkgs" "${DEBIAN_CONDA_ENV_PATH}/pkgs"

WORKDIR ${WORK_DIR}

# Add control file
RUN dpkg-gencontrol \
    -P${DEBIAN_PACKAGE_TREE} \
    -f${DEBIAN_PACKAGE_TREE}/DEBIAN/files

# Actual debian packaging
RUN ls -lh ${DEBIAN_PACKAGE_TREE}/DEBIAN/
RUN ls -lh ${DEBIAN_PACKAGE_TREE}/
RUN fakeroot dpkg-deb --build ${DEBIAN_PACKAGE_TREE} ${DEB_FILE}

# Cleanup
RUN rm -Rf ${DEBIAN_PACKAGE_TREE} ${WORK_DIR}/airflow.tgz

CMD ["/bin/bash"]



#
# == Stage 5. test_deb - A 'test' image with the .deb installed
#
FROM docker-registry.wikimedia.org/bullseye:${BULLSEYE_VERSION} as test_deb

# Declare the ARGs used for this build stage.
ARG DEB_FILE

# Dumb bug: https://github.com/debuerreotype/debuerreotype/issues/10
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN seq 1 8 | xargs -I{} mkdir -p /usr/share/man/man{}

# Get apt dependencies
RUN apt-get update && \
  apt-get install -y --no-install-recommends vim \
                                             wget \
                                             krb5-config \
                                             libkrb5-dev \
                                             libsasl2-dev \
                                             sasl2-bin \
                                             libsasl2-modules-gssapi-mit \
                                             dpkg && \
  rm -rf /var/lib/apt/lists/*

# Copy and install the .deb from the previous build stage
COPY --from=build_deb ${DEB_FILE} ${DEB_FILE}
RUN dpkg -i ${DEB_FILE}

CMD ["/bin/bash"]



#
# == Stage 6: linting - Build conda environment containing only linting deps
#
FROM conda_dist as conda_env_lint

# Declare the ARGs used for this build stage.
ARG WORK_DIR
ARG PACKAGE_NAME
ARG PYTHON_VERSION

# Get dependencies files to build the production Conda environment
RUN mkdir -p "${WORK_DIR}"
WORKDIR ${WORK_DIR}
COPY pyproject.toml "${WORK_DIR}/"

# Create the production Conda environment
RUN source /opt/miniconda/bin/activate && \
  conda create --name ${PACKAGE_NAME} python==${PYTHON_VERSION} && \
  conda activate ${PACKAGE_NAME} && \
  pip install --quiet .[lint] && \
  rm -Rf /opt/miniconda/pkgs



#
# == Stage 7: publish_deb - Publish deb package to registry
#
FROM docker-registry.wikimedia.org/bullseye:${BULLSEYE_VERSION} as publish_deb

# Declare the ARGs used for this build stage.
ARG DEB_FILE
ARG PACKAGE_NAME
ARG PACKAGE_VERSION

# Provided by Gitlab-CI
ARG CI_JOB_TOKEN=tbd_ci_job_token
ARG GENERIC_PACKAGE_REGISTRY_URI=tbd_generic_package_registry_uri

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl ca-certificates && \
    rm -rf /var/lib/apt/lists/*

COPY --from=build_deb ${DEB_FILE} ${DEB_FILE}

RUN echo "CI_JOB_TOKEN: $CI_JOB_TOKEN" && \
    echo "GENERIC_PACKAGE_REGISTRY_URI: $GENERIC_PACKAGE_REGISTRY_URI" && \
    echo "DEB_FILE: $DEB_FILE" && \
    ls -lh ${DEB_FILE} && \
    echo "Uploading to: ${GENERIC_PACKAGE_REGISTRY_URI}/${PACKAGE_NAME}/${PACKAGE_VERSION}/${DEB_FILE##*/}" && \
    curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${DEB_FILE}" "${GENERIC_PACKAGE_REGISTRY_URI}/${PACKAGE_NAME}/${PACKAGE_VERSION}/${DEB_FILE##*/}"
