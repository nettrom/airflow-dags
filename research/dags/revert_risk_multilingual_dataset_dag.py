import json
from dataclasses import field
from datetime import datetime, timedelta
from pathlib import Path
from typing import Final

from airflow import DAG
from airflow.models import Variable
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from pydantic.dataclasses import dataclass
from pydantic.json import pydantic_encoder
from typing_extensions import Self

from research.config import dag_config
from research.dags.snapshot_sensor import wait_for_mediawiki_history_snapshot
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

WIKI_BATCHES: Final = [
    [
        "enwiki",
    ],
    [
        "dewiki",
        "eswiki",
        "frwiki",
        "itwiki",
        "jawiki",
        "plwiki",
        "ruwiki",
        "zhwiki",
    ],
    [
        "arwiki",
        "azwiki",
        "bgwiki",
        "bnwiki",
        "cawiki",
        "cswiki",
        "dawiki",
        "elwiki",
        "eowiki",
        "etwiki",
        "euwiki",
        "fawiki",
        "fiwiki",
        "hewiki",
        "hiwiki",
        "hrwiki",
        "huwiki",
        "hywiki",
        "idwiki",
        "kawiki",
        "kowiki",
        "ltwiki",
        "lvwiki",
        "mswiki",
        "nlwiki",
        "nowiki",
        "rowiki",
        "simplewiki",
        "skwiki",
        "slwiki",
        "srwiki",
        "svwiki",
        "tawiki",
        "thwiki",
        "trwiki",
        "ukwiki",
        "urwiki",
        "uzwiki",
        "viwiki",
    ],
]


@dataclass(frozen=True)
class DagProperties:
    # Dag configuration
    alerts_email: str = "research-engineering-alerts@lists.wikimedia.org"
    conda_env: str = dag_config.artifact("revert_risk_datasets.tgz")
    sla: timedelta = timedelta(days=3)
    # Pipeline arguments
    snapshot: str = "{{ data_interval_start | to_ds_month }}"
    start_time: str = "{{ data_interval_start | to_ds }}"
    end_time: str = "{{ data_interval_end | to_ds }}"
    wiki_batches: list[list[str]] = field(default_factory=lambda: WIKI_BATCHES)
    output_path: Path = (
        # The job in this DAG is configured to write its output
        # partitioned by wikis. So each batch job will write to the same
        # path with different a subdir for each wiki in the batch.
        Path(dag_config.hdfs_temp_directory)
        / "revert_risk_datasets"
        / "multilingual"
        / "{{ data_interval_start | to_ds_nodash }}_{{ data_interval_end | to_ds_nodash }}"
    )
    # Spark configuration
    # This takes up ~20% resources of the cluster as of Sep, 2023.
    # Each batch in WIKI_BATCHES gets processed in ~2 hours.
    # The relatively high resource usage is because this job runs multiple
    # joins involving full wikitext and revision histories. Fewer resources might
    # still get the job done but with failures and retries.
    driver_memory: str = "8G"
    executor_cores: int = 2
    executor_memory: str = "24G"
    executor_memory_overhead: str = "4G"
    max_executors: int = 98

    @classmethod
    def from_variable(cls, variable_key: str) -> Self:
        """Load properties from the airflow variable associated with `variable_key`.
        If no value is found for the given key, the variable will be initialized
        with the defaults in `DagProperties`.
        """
        # Get the current value of the variable or initialize with
        # an empty dict and store that as the value in json
        variable_value = Variable.setdefault(variable_key, default=dict(), deserialize_json=True)

        # Initialize DagProperties and override properties found in value
        dag_properties = cls(**variable_value)

        # Set dag_properties as the new variable value after serializing it to json.
        dag_properties_json = json.dumps(dag_properties, indent=4, default=pydantic_encoder)
        Variable.update(key=variable_key, value=dag_properties_json)

        return dag_properties


props = DagProperties.from_variable("revert_risk_multilingual_dataset")
default_args = dag_config.default_args | {"email": props.alerts_email, "sla": props.sla}


with DAG(
    dag_id="revert_risk_multilingual_dataset",
    schedule="@monthly",
    start_date=datetime(2023, 8, 1),
    user_defined_filters=filters,
    default_args=default_args,
    tags=("research", "revertrisk"),
    catchup=False,
) as dag:
    # Unlike wmf.mediawiki_wikitext_history, wmf.mediawiki_history is
    # not partitioned by wiki so we wait for it once at the start of the
    # worflow instead of individually for every wiki batch.
    workflow = wait_for_mediawiki_history_snapshot(props.snapshot)

    for batch_no, wikis in enumerate(props.wiki_batches, start=1):
        wiki_partitions = (f"wiki_db={wiki}" for wiki in wikis)

        # make partitions for wikitext history using wiki partitions
        # e.g. wmf.mediawiki_wikitext_history/snapshot=2023-08/wiki_db=enwiki
        wikitext_history_partitions = [
            f"wmf.mediawiki_wikitext_history/snapshot={props.snapshot}/{wiki_partition}"
            for wiki_partition in wiki_partitions
        ]
        wait_for_wikitext_history = NamedHivePartitionSensor(
            task_id=f"wait_for_wmf.mediawiki_wikitext_history_{batch_no}",
            partition_names=wikitext_history_partitions,
            poke_interval=timedelta(hours=1).total_seconds(),
        )

        revert_risk_multilingual_args = {
            "--snapshot": props.snapshot,
            "--wikis": ",".join(wikis),
            "--start-time": props.start_time,
            "--end-time": props.end_time,
            "--output": str(props.output_path),
        }

        spark_conf = {
            "spark.sql.sources.partitionOverwriteMode": "dynamic",
            "spark.sql.shuffle.partitions": 4096,
            "spark.sql.adaptive.enabled": "true",
            "spark.shuffle.io.maxRetries": 10,
            "spark.task.maxFailures": 10,
        }

        generate_dataset = SparkSubmitOperator.for_virtualenv(
            task_id=f"generate_dataset_{batch_no}",
            virtualenv_archive=props.conda_env,
            entry_point="bin/revert_risk_datasets_multilingual.py",
            application_args=revert_risk_multilingual_args,
            driver_memory=props.driver_memory,
            executor_cores=props.executor_cores,
            executor_memory=props.executor_memory,
            executor_memory_overhead=props.executor_memory_overhead,
            max_executors=props.max_executors,
            conf=spark_conf,
        )

        # NOTE: currently the batches get processed sequentially, which is
        # fine since we don't want to consume too many resources at once but
        # if one batch fails for some reason, the rest will too as the default
        # trigger rule for a task is all_success.
        workflow = workflow >> wait_for_wikitext_history >> generate_dataset
